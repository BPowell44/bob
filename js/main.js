// Initialise Phaser and sets the screen size to 400x490px game
var game = new Phaser.Game(400, 490, Phaser.AUTO, 'gameDiv');

// Creates a main state which contains the game
var mainState = {

    // acts as the preloader state, loads images ready for the game
    preload: function() {

      // using scale manager you can change how the game fits the screen
      this.scale.scaleMode = Phaser.ScaleManager.NO_SCALE;

      //have the game centered horizontally
      this.scale.pageAlignHorizontally = true;
      this.scale.pageAlignVertically = true;

      //screen size will be set automatically
      this.scale.setScreenSize(true);

        // Changes the background colour to this hex colour
        game.stage.backgroundColor = '#71c5cf';

        // Loads in the sprite called bird
        game.load.image('bird', 'assets/bird.png');

        // Loads in the sprite called pipe
        game.load.image('pipe', 'assets/pipe.png');
    },

    // This part would usually be in the Game state
    create: function() {

        // Sets the simplest phaser physics system
        game.physics.startSystem(Phaser.Physics.ARCADE);

        // Displays the bird(block) at this location
        this.bird = this.game.add.sprite(100, 245, 'bird');

        // Using the physics system, add a artifical gravity, causing the bird to go down in the y axis
        game.physics.arcade.enable(this.bird);
        this.bird.body.gravity.y = 1000;

        // Waits for input from spacebar, when pressed run the Jump function
        var spaceKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        spaceKey.onDown.add(this.jump, this);

        // Creates a group to where you can create mulitple instances of one thing, 20 have been made, works like the pooling system in unity
        this.pipes = game.add.group();
        this.pipes.enableBody = true;

        // This is where it creates 20 instances of pipe for the group
        this.pipes.createMultiple(20, 'pipe');

        // Timer counts to 1.5 seconds and then calls the function addRowOfPipes. This is looped
        this.timer = this.game.time.events.loop(1500, this.addRowOfPipes, this);

        // Adds the number 0 at 20, 20 units away from the top left corner, 0 is being scored in a variable to be updated later on for the score
        this.score = 0;
        this.labelScore = this.game.add.text(20, 20, "0", { font: "30px Arial", fill: "#ffffff" });
    },

    // Just like in unity, Update is used when you want to check something constantly. Checks 60 times per second
    update: function() {

        // If the bird leaves the game world (inWorld) then the game calls the Restart function. inWorld is the screen size you see
        if (this.bird.inWorld == false)
            this.restartGame();

        // overlap isnt a collider, it has no physics applied, just checks if the two objects have overlapped each other
        // if they have then the RestartGame function is called
        game.physics.arcade.overlap(this.bird, this.pipes, this.restartGame, null, this);
    },

    // the function which gives the bird flight
    jump: function() {

        // Adds a vertical velocity to the bird in the y axis, which overcomes the gravity
        this.bird.body.velocity.y = -350;
    },

    // The function which restarts the game
    restartGame: function() {

        // This will start the main state, which will cause the game to start from the very beginning
        game.state.start('main');
    },

    // This function is used to add a pipe onto the screen
    addOnePipe: function(x, y) {

        // In the group of pipes (pooling) reinstance the last pipe which went off screen
        var pipe = this.pipes.getFirstDead();

        // Then set it to this position
        pipe.reset(x, y);

        // Adds velocity to the pipe to make it move left towards the player
        pipe.body.velocity.x = -200;

        // Just like when the bird goes out of screen, the pipes get recycled and are reused when called upon
        pipe.checkWorldBounds = true;
        pipe.outOfBoundsKill = true;
    },

    // Adds a row of 6 pipes with a random hole somewhere in it
    addRowOfPipes: function() {
        // This creates a value which will be random to be used to create the hole in the pipes
        var hole = Math.floor(Math.random()*5)+1;

        // Using a for loop to place a pipe and then move to the next position and either miss or place pipe etc
        for (var i = 0; i < 8; i++)
            if (i != hole && i != hole +1)
                this.addOnePipe(400, i*60+10);

        // Every time a row of pipes is added, the players score goes up by one and is displayed
        this.score += 1;
        this.labelScore.text = this.score;
    },
};

// Adds the main state and the starts the main state
game.state.add('main', mainState);
game.state.start('main');
